import org.jetbrains.gradle.ext.ProjectSettings
import org.jetbrains.gradle.ext.TaskTriggersConfig
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.8.10"
    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("org.jetbrains.gradle.plugin.idea-ext") version "1.1.6"
    `maven-publish`
}

group = "de.limited_dev.modpackinstaller"
version = System.getenv("CI_COMMIT_TAG")?.let { "$it-${System.getenv("CI_COMMIT_SHORT_SHA")}-prod" }
    ?: System.getenv("CI_COMMIT_SHORT_SHA")?.let { "$it-dev" }
            ?: "devbuild"


val fabricDownloadLocation =
    "https://maven.fabricmc.net/net/fabricmc/fabric-installer/0.11.2/fabric-installer-0.11.2.jar"
//https://fileshare.limited-dev.de:4443/share-download/4e6935a2-6ba8-4131-9141-29197ce9dc18
//http://192.168.178.200:5091/share?id=70853092-8026-4220-ba8f-3cb6c520df75
val modpackDownloadLocation =
    "https://cdn.discordapp.com/attachments/996483339714306089/1084462076673658990/Blooper_Build_Modpack.zip"
var mcversion = "1.19.2"


val mavenArtifact = "ModpackInstaller"
project.base.archivesName.set(mavenArtifact)

repositories {
    mavenCentral()
    mavenLocal()
    maven(uri("https://oss.sonatype.org/content/repositories/snapshots/"))
    maven {
        name = "Gitlab"
        val projectId = System.getenv("CI_PROJECT_ID")
        val apiV4 = System.getenv("CI_API_V4_URL")
        url = uri("https://$apiV4/projects/$projectId/packages/maven")
        authentication {
            create("header", HttpHeaderAuthentication::class.java) {
                if (System.getenv("CI_JOB_TOKEN") != null) {
                    credentials(HttpHeaderCredentials::class) {
                        name = "Job-Token"
                        value = System.getenv("CI_JOB_TOKEN")
                    }
                } else {
                    credentials(HttpHeaderCredentials::class) {
                        name = "Private-Token"
                        value = project.ext["myGitlabToken"] as String
                    }
                }
            }
        }
    }
}

dependencies {
    implementation("com.formdev:flatlaf:3.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
}

val targetJavaVersion = 1.8
val templateSrc = project.rootDir.resolve("src/main/templates")
val templateDest = project.buildDir.resolve("generated/templates")
val templateProps = mapOf(
    "version" to project.version as String,
    "mcversion" to mcversion,
    "fabricDownloadLocation" to fabricDownloadLocation,
    "modpackDownloadLocation" to modpackDownloadLocation
)

tasks {
    create<Copy>("generateTemplates") {
        filteringCharset = "UTF-8"

        inputs.properties(templateProps)
        from(templateSrc)
        expand(templateProps)
        into(templateDest)
    }

    withType<Jar> {
        manifest {
            attributes["Main-Class"] = "de.limited_dev.modpackinstaller.MainKt"
        }
        // To add all of the dependencies
        from(sourceSets.main.get().output)

        dependsOn(configurations.runtimeClasspath)
        from({
            configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
        })
        duplicatesStrategy = DuplicatesStrategy.INCLUDE
        dependsOn("generateTemplates", "processResources")
    }

    withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
        dependencies {
            include(dependency("com.formdev:flatlaf:3.0"))
            include(dependency("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4"))
        }
        dependsOn("generateTemplates", "processResources")
    }

    withType<JavaCompile> {
        // ensure that the encoding is set to UTF-8, no matter what the system default is
        // this fixes some edge cases with special characters not displaying correctly
        // see http://yodaconditions.net/blog/fix-for-java-file-encoding-problems-with-gradle.html
        // If Javadoc is generated, this must be specified in that task too.
        options.encoding = "UTF-8"
        if (targetJavaVersion >= 10 || JavaVersion.current().isJava10Compatible) {
            options.release.set(8)
        }


        dependsOn("generateTemplates", "processResources")
    }

    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = targetJavaVersion.toString()

        dependsOn("generateTemplates", "processResources")
    }

    withType<Jar> {
        from("LICENSE") {
            rename { "${it}_${project.base.archivesName.get()}" }
        }

        archiveBaseName.set(mavenArtifact)

        dependsOn("generateTemplates", "processResources")
    }
}


java {
    val javaVersion = JavaVersion.toVersion(targetJavaVersion)
    if (JavaVersion.current() < javaVersion) {
        toolchain.languageVersion.set(JavaLanguageVersion.of(targetJavaVersion.toString()))
    }


    withSourcesJar()
}

sourceSets {
    main {
        java {
            srcDir(templateDest)
        }
    }
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            version = project.version as String
            artifactId = mavenArtifact
            from(components["java"])
        }
    }

    repositories {
        if (System.getenv("CI_JOB_TOKEN") != null) {
            maven {
                name = "GitLab"
                val projectId = System.getenv("CI_PROJECT_ID")
                val apiV4 = System.getenv("CI_API_V4_URL")
                url = uri("$apiV4/projects/$projectId/packages/maven")
                authentication {
                    create("token", HttpHeaderAuthentication::class.java) {
                        credentials(HttpHeaderCredentials::class.java) {
                            name = "Job-Token"
                            value = System.getenv("CI_JOB_TOKEN")
                        }
                    }
                }
            }
        }
    }
}

rootProject.idea.project {
    this as ExtensionAware
    configure<ProjectSettings> {
        this as ExtensionAware
        configure<TaskTriggersConfig> {
            afterSync(tasks["generateTemplates"], tasks["processResources"])
        }
    }
}

//rootProject.eclipse.synchronizationTasks("generateTemplates", "processResources")
//Fuck eclipse users amirite?
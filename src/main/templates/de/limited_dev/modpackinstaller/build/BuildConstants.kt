package de.limited_dev.modpackinstaller.build

internal object BuildConstants {
    const val version = "${version}"
    const val mcversion = "${mcversion}"
    const val fabricDownloadLocation = "${fabricDownloadLocation}"
    const val modpackDownloadLocation = "${modpackDownloadLocation}"
}

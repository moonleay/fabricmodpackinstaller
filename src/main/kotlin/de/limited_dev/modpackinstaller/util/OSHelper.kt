/*
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.modpackinstaller.util

import java.io.File
import java.util.*

enum class OSHelper(val oSType: String) {
    WINDOWS(System.getenv("APPDATA") + File.separator + ".minecraft"),
    MAC("Please select your .minecraft folder"),
    LINUX(System.getProperty("user.home") + File.separator + ".minecraft");

    companion object {
        val os: OSHelper
            get() {
                val currentOS = System.getProperty("os.name").lowercase(Locale.getDefault())
                if (currentOS.startsWith("windows")) {
                    return WINDOWS
                } else if (currentOS.startsWith("mac")) {
                    return MAC
                }
                return LINUX
            }
        val dotMinecraftLocation: String
            get() = System.getProperty("os.name")
    }
}
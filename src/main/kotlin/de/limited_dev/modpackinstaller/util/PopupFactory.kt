/*
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.modpackinstaller.util

import javax.swing.JOptionPane

object PopupFactory {

    fun getOk(title: String, msg: String) {
        println("Popup created")
        val res = JOptionPane.showConfirmDialog(
            null,
            msg,
            title,
            JOptionPane.CLOSED_OPTION
        )
    }

    fun getYesOrNoError(title: String, msg: String): Boolean {
        println("Popup created")
        val res = JOptionPane.showConfirmDialog(
            null,
            msg,
            title,
            JOptionPane.OK_CANCEL_OPTION
        )
        if (res == 0) {
            println("Pressed OK")
            return true
        } else {
            println("Pressed CANCEL")
            return false
        }
    }

}
/*
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.modpackinstaller.gui

import de.limited_dev.modpackinstaller.backend.Manager
import de.limited_dev.modpackinstaller.util.OSHelper
import de.limited_dev.modpackinstaller.util.PopupFactory
import java.awt.*
import java.io.File
import javax.swing.*
import javax.swing.border.EmptyBorder


object MainPanel : JPanel() {
    private val generatorButton = JButton("Download & Install")
    private var fabricLabel: JLabel
    private var modpackLabel: JLabel
    private var dotMcLocationBar: JTextField
    private var dotMcLocation: String = OSHelper.os.oSType

    init {
        val layout = GridLayout(6, 1)

        this.layout = layout
        border = EmptyBorder(10, 10, 10, 10)

        val info = JLabel("Modpack Installer")
        info.horizontalAlignment = SwingConstants.CENTER
        info.font = Font(font.name, font.style, font.size + 6)
        this.add(info, BorderLayout.NORTH)

        val flayout = FlowLayout()
        flayout.hgap = 10
        flayout.alignment = SwingConstants.VERTICAL

        fabricLabel = JLabel("Fabric mod loader")
        modpackLabel = JLabel("Modpack")

        this.add(fabricLabel)
        this.add(modpackLabel)

        dotMcLocationBar = JTextField(dotMcLocation)
        dotMcLocationBar.isEditable = false
        this.add(dotMcLocationBar)

        val openFileChooser = JButton("Select .minecraft Folder")
        openFileChooser.addActionListener {
            dotMcLocation = promptForFolder(openFileChooser)
            dotMcLocationBar.text = dotMcLocation
        }
        this.add(openFileChooser)
        generatorButton.addActionListener {
            val dotMcMods = File(dotMcLocation + File.separator + "mods")
            if (dotMcMods.exists()) {
                val bool = PopupFactory.getYesOrNoError(
                    "Warning! .minecraft/mods folder found",
                    "This installer will clear your mods folder in order to install the right Modpack."
                )
                if (bool) {
                    generatorButton.isEnabled = false
                    Manager.startDownloadAndInstall(dotMcLocation)
                }
            } else {
                generatorButton.isEnabled = false
                Manager.startDownloadAndInstall(dotMcLocation)
            }
        }
        this.add(generatorButton)
    }

    private fun promptForFolder(parent: Component?): String {
        val fc = JFileChooser()
        fc.fileSelectionMode = JFileChooser.DIRECTORIES_ONLY
        return if (fc.showOpenDialog(parent) == JFileChooser.APPROVE_OPTION) {
            fc.selectedFile.absolutePath
        } else "Error"
    }

    fun updateFabricModloader(tx: String) {
        fabricLabel.text = "Fabric mod loader $tx"
    }

    fun updateModpack(tx: String) {
        modpackLabel.text = "Modpack $tx"
    }
}
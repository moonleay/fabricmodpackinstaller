/*
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.modpackinstaller.gui

import de.limited_dev.modpackinstaller.build.BuildConstants
import java.awt.BorderLayout
import javax.swing.JFrame
import javax.swing.WindowConstants

class GUI : JFrame("Modpack installer " + BuildConstants.version) {
    init {
        this.isResizable = false
        this.defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE

        val contentPane = this.contentPane
        contentPane.layout = BorderLayout()

        contentPane.add(MainPanel, BorderLayout.CENTER)
        contentPane.add(BottomBar(), BorderLayout.SOUTH)

        this.pack()
        this.setLocationRelativeTo(null)
        this.isVisible = true

    }

}
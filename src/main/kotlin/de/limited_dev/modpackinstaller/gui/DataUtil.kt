/*
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.modpackinstaller.gui

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.net.URL
import java.util.zip.ZipFile

object DataUtil {
    suspend fun downloadFile(url: String, downloadLocation: String, isLoader: Boolean): Double {
        var bytesDownloaded = 0L
        withContext(Dispatchers.IO) {
            val connection = URL(url).openConnection()
            val stream = BufferedInputStream(connection.getInputStream())
            val outputStream = BufferedOutputStream(FileOutputStream(File(downloadLocation)))
            val data = ByteArray(1024)
            var bytesRead = stream.read(data, 0, 1024)
            while (bytesRead != -1) {
                outputStream.write(data, 0, bytesRead)
                bytesDownloaded += bytesRead
                if (isLoader)
                    MainPanel.updateFabricModloader("Downloading " + bytesDownloaded.toString() + "MB")
                else
                    MainPanel.updateModpack("Downloading " + bytesDownloaded.toString() + "MB")
                bytesRead = stream.read(data, 0, 1024)
            }
            outputStream.close()
            stream.close()
        }
        return bytesDownloaded / 1024.0 / 1024.0
    }

    fun unzip(zipFile: File, destinationDirectory: File) {
        // Create the destination directory if it doesn't exist
        if (!destinationDirectory.exists()) {
            destinationDirectory.mkdirs()
        }

        // Open the ZIP file and iterate over its entries
        val zip = ZipFile(zipFile)
        val entries = zip.entries()
        while (entries.hasMoreElements()) {
            val entry = entries.nextElement()

            // Extract the entry to the specified destination directory
            val entryDestination = File(destinationDirectory, entry.name)
            if (entry.isDirectory) {
                entryDestination.mkdirs()
            } else {
                entryDestination.parentFile.mkdirs()
                zip.getInputStream(entry).use { input ->
                    entryDestination.outputStream().use { output ->
                        input.copyTo(output)
                    }
                }
            }
        }

        // Close the ZIP file
        zip.close()
    }
}
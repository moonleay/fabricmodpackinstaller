/*
 *     Copyright (C) 2023  limited_dev
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.limited_dev.modpackinstaller.backend

import de.limited_dev.modpackinstaller.build.BuildConstants
import de.limited_dev.modpackinstaller.gui.DataUtil
import de.limited_dev.modpackinstaller.gui.DataUtil.downloadFile
import de.limited_dev.modpackinstaller.gui.MainPanel
import de.limited_dev.modpackinstaller.util.PopupFactory
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File
import kotlin.system.exitProcess

object Manager {
    fun startDownloadAndInstall(dotMcLocation: String) {
        val tempFolder = File(dotMcLocation + File.separator + "temp")
        val modFolder = File(dotMcLocation + File.separator + "mods")
        Runtime.getRuntime().addShutdownHook(Thread {
            println("Application is shutting down")
            val tempFolder2 = File(dotMcLocation + File.separator + "temp")
            if (tempFolder.exists())
                tempFolder2.deleteRecursively()
        })
        if (tempFolder.exists())
            tempFolder.deleteRecursively()
        tempFolder.mkdirs()

        val modloaderCoroutine = GlobalScope.launch {
            println("Downloading Modloader")
            val megabytesDownloaded = runBlocking {
                downloadFile(
                    BuildConstants.fabricDownloadLocation,
                    dotMcLocation + File.separator + "temp" + File.separator + "fabric.jar",
                    true
                )
            }
            println("Downloaded $megabytesDownloaded MB Modloader")
            MainPanel.updateFabricModloader("Installing...")
            val command = "java"
            val args = listOf(
                "-jar",
                "fabric.jar",
                "client",
                "-mcversion",
                BuildConstants.mcversion
            )

            val processBuilder = ProcessBuilder(command, *args.toTypedArray())
                .directory(tempFolder)
                .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                .redirectError(ProcessBuilder.Redirect.INHERIT)

            val process = processBuilder.start()
            val exitCode = process.waitFor()
            MainPanel.updateFabricModloader("Done!")
            println("Java application exited with code: $exitCode")
        }

        val modpackCoroutine = GlobalScope.launch {
            println("Downloading Modpack")
            val megabytesDownloaded = runBlocking {
                downloadFile(
                    BuildConstants.modpackDownloadLocation,
                    dotMcLocation + File.separator + "temp" + File.separator + "pack.zip",
                    false
                )
            }
            println("Downloaded $megabytesDownloaded MB Modpack\nUnzipping...")

            MainPanel.updateModpack("Unzipping...")
            var modzip = File(tempFolder.absolutePath + File.separator + "pack.zip")
            DataUtil.unzip(modzip, modFolder)
            MainPanel.updateModpack("Done!")
            println("Done!")
        }

        runBlocking {
            modloaderCoroutine.join()
            modpackCoroutine.join()
        }
        println("Done.")
        PopupFactory.getOk(
            "Installed!",
            "The Modpack is now installed!\nYou may now (re)open your Minecraft Launcher!\n\nHave fun playing! ~limited_dev"
        )
        exitProcess(0)
    }
}
